<?php

namespace App\Command;

use App\Utils\PropositionsOperations;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:set-propositions-dates')]
class SetPropositionsDateCommand extends Command
{
    protected static $defaultName = 'app:set-propositions-dates';

    private EntityManagerInterface $entityManager;
    private PropositionsOperations $propositionsOperations;

    public function __construct(EntityManagerInterface $entityManager, PropositionsOperations $propositionsOperations)
    {
        $this->entityManager = $entityManager;
        $this->propositionsOperations = $propositionsOperations;
        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $question = new ChoiceQuestion('How do you want to send propositions ? Type 1 to send by arrival order and type 2 to send by score priority.', ['By arrival','By score'], 0);
        $question->setErrorMessage('Invalid choice, please type 1 or 2 !');
        $mode = $helper->ask($input, $output, $question);
        if($mode == 'By arrival'){
            $mode = 0;
        }
        if($mode == 'By score'){
            $mode = 1;
        }

        $question = new Question('Type the maximum date allowed to send propositions (YYYY-MM-DD).');
        $maxDate = $helper->ask($input, $output, $question);

        try {
            $maxDate = new \DateTime($maxDate);
        }
        catch (Exception $e) {
            echo "Invalid date !";
            return Command::FAILURE;
        }

        if($maxDate < new \DateTime('now')){
            echo "Cant set a past date !";
            return Command::FAILURE;
        }

        $question = new Question('Type the minimum number of propositions to send by wave.');
        $minNbPropositionsWave = $helper->ask($input, $output, $question);



        if(!is_numeric($minNbPropositionsWave)){
            echo "Please type a number !";
            return Command::FAILURE;
        }

        if($minNbPropositionsWave < 1 || ((int)$minNbPropositionsWave) != $minNbPropositionsWave){
            echo "Please type a positive integer number !";
            return Command::FAILURE;
        }


        $em = $this->entityManager;
        $repository = $em->getRepository("App:Proposition");

        $propositions = $repository->findAll();

        foreach ($propositions as $proposition){
            echo $proposition->getSendDate()->format('Y-m');
        }

        try {
            $datedPropositions = $this->propositionsOperations->setSendDates($propositions, $maxDate, $minNbPropositionsWave, (int)$mode);
        } catch (Exception $e) {
            echo $e->getMessage();
            return Command::FAILURE;
        }

        $question = new ConfirmationQuestion('Flush result ? (y/n) default : n', false);

        if ($helper->ask($input, $output, $question)) {
            foreach ($datedPropositions as $proposition){
                $em->persist($proposition);
            }
            $em->flush();
            echo "Propositions saved ! ";
        }

        return Command::SUCCESS;
    }
}
