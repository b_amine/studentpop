<?php

namespace App\DataFixtures;

use App\Entity\Proposition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i < 100; $i++){
            $proposition = new Proposition(rand(0,100), 'Mission message', rand(1,100), 'sondage');
            $manager->persist($proposition);
        }

        $manager->flush();
    }
}
