<?php

namespace App\Utils;

use DateTime;
use Exception;

class PropositionsOperations
{

    /**
     * @throws Exception
     */
    public function setSendDates(array $propositions, DateTime $maxDate, int $minNbPropositionsWave, int $mode = 0): array
    {
        //data validation
        if($maxDate < new \DateTime('now')){
            throw new Exception("Cant set a past date !");
        }

        if(!is_numeric($minNbPropositionsWave)){
            throw new Exception("Please type a number !");
        }

        if($minNbPropositionsWave < 1 || !is_integer($minNbPropositionsWave)){
            throw new Exception("Please type a positive integer number !");
        }

        if(!in_array($mode,[0,1])){
            throw new Exception("Invalid mode");
        }

        $nbPropositions = count($propositions);

        //Count difference in minutes to count the number or propositions per wave

        $difference = date_diff(new DateTime('now'), $maxDate);
        $minutes = $difference->days * 24 * 60;
        $minutes += $difference->h * 60;
        $minutes += $difference->i;

        $maxNbWaves = floor($minutes/5);

        //Number of missions per wave ( except the last wave potentially )
        $nbPropositionsPerWave = max( ceil($nbPropositions / $maxNbWaves) , $minNbPropositionsWave);

        if($mode == 0) {
            $nbWaves = 0;

            $sendDate = new DateTime('now');
            for ($i = 0; $i < $nbPropositions; $i++) {
                if ($i % $nbPropositionsPerWave == 0) {
                    $nbWaves++;

                    //Add 5 minutes or go to next 8h00 if between 23h & 8h
                    if ((int)$sendDate->format('H') == 22 && (int)$sendDate->format('i') > 55) {
                        $sendDate->modify('tomorrow')->setTime(8, 0);
                    } elseif ((int)$sendDate->format('H') < 8) {
                        $sendDate->setTime(8, 0);
                    } else {
                        $sendDate->modify('+5 minutes');
                    }
                    echo "--NEXT WAVE--\n";
                }
                $propositions[$i]->setSendDate($sendDate);
                echo $propositions[$i]->getSendDate()->format('Y-m-d H:i:s') . "---" . $propositions[$i]->getScore() . "---" . "\n";
            }

            echo "Number of propositions : " . $nbPropositions . "\n";
            echo "Number of waves : " . $nbWaves . "\n";
            echo "Number of propositions per wave ( last wave ) : " . $nbPropositionsPerWave . " ( " . $nbPropositions % $nbPropositionsPerWave . " )\n";

        }else {

            $minScore = 0;
            $maxScore = 100;

            //place in waves
            $waves = array();

            foreach ($propositions as $proposition) {
                $targetWave = ceil($maxNbWaves - ($maxNbWaves * ( $proposition->getScore() - $minScore ) / ($maxScore - $minScore + 1)));
                $waves[$targetWave][] = $proposition;
            }

            $nbWaves = count($waves);

            //fill dates
            $sendDate = new DateTime('now');

            //sort for readability in console output - can be removed
            ksort($waves);

            foreach ($waves as $wave) {
                //Add 5 minutes or go to next 8h00 if between 23h & 8h
                if ((int)$sendDate->format('H') == 22 && (int)$sendDate->format('i') >= 55) {
                    $sendDate->modify('tomorrow')->setTime(8, 0);
                } elseif ((int)$sendDate->format('H') < 8) {
                    $sendDate->setTime(8, 0);
                } else {
                    $sendDate->modify('+5 minutes');
                }

                //Set date
                foreach ($wave as $proposition) {
                    $proposition->setSendDate($sendDate);
                    echo $proposition->getSendDate()->format('Y-m-d H:i:s') . "- SCORE -" . $proposition->getScore() . "---" . "\n";
                }
                echo "--NEXT WAVE--\n";
            }

            echo "Number of propositions : " . $nbPropositions . "\n";
            echo "Number of waves : " . $nbWaves . "\n";
        }
        return $propositions;
    }
}