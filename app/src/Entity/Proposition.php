<?php

namespace App\Entity;

use App\Repository\PropositionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PropositionRepository::class)]
class Proposition
{
    const SHORT_MISSION = 'mission courte';
    const LONG_MISSION = 'mission longue';
    const SURVEY = 'sondage';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private int $userId;

    #[ORM\Column(type: 'string', length: 255)]
    private string $message;

    #[ORM\Column(type: 'integer')]
    private int $score;

    #[ORM\Column(type: 'string', length: 20)]
    private string $missionType;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $sendDate;

    /**
     * @param $id
     * @param int $userId
     * @param string $message
     * @param int $score
     * @param string $missionType
     */
    public function __construct(int $userId, string $message, int $score, string $missionType)
    {
        $this->userId = $userId;
        $this->message = $message;
        $this->score = $score;
        $this->missionType = $missionType;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        if ($score > 100 || $score < 0) {
            throw new \InvalidArgumentException("Invalid score, value has to be between 0 & 100");
        }
        $this->score = $score;

        return $this;
    }

    public function getMissionType(): ?string
    {
        return $this->missionType;
    }

    public function setMissionType(string $missionType): self
    {
        if (!in_array($missionType, array(self::SHORT_MISSION, self::LONG_MISSION, self::SURVEY))) {
            throw new \InvalidArgumentException("Invalid mission type");
        }
        $this->missionType = $missionType;

        return $this;
    }

    public function getSendDate(): ?\DateTimeInterface
    {
        return $this->sendDate;
    }

    public function setSendDate(?\DateTimeInterface $sendDate): self
    {
        $this->sendDate = $sendDate;

        return $this;
    }
}
